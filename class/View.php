<?php

class View
{

    function error(Exception $e){
        $this->start();
        ?>
        <div class="alert alert-danger" role="alert">
       На сайте проблемы. Зайдите, пожалуйста, позже.
<br>
            <?echo $e->getMessage();?>
            <br>
            <pre><?echo $e->getTraceAsString();?></pre>
        </div>

        <?php
        $this->end();


    }

    function start()
    {
        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
                  integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                  crossorigin="anonymous">

            <title>Report</title>
        </head>
        <body>
        <h1>Отчет по сотрудникам</h1>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>
        <script>
            $(function () {
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            })
        </script>

        <?php
    }

    function end()
    {
        ?>
        </body>
        </html>

        <?php
    }


    public function resolve($arResult)
    {

        $this->start();
       // print_r('<pre>');
       // print_r($arResult);
       // print_r('</pre>');

        if (
        !empty($arResult["ERROR"])

        ) {
            ?>
            <div class="alert alert-danger" role="alert">
            <?php echo join($arResult["ERROR"], ", ") ?>
            </div><?php
        }

        ?>


        <form method="post">
            <div class="form-group">
                <label for="dateFrom">Составить отчет от: </label>


                <input type="date" id="dateFrom" required="required"
                       value="<?php echo $arResult["FIELDS"]['dateFrom'] ?>" name="dateFrom" class="form-control">

                <label for="dateTo">Составить отчет до:</label>

                <input type="date" id="dateTo" name="dateTo" value="<?php echo $arResult["FIELDS"]['dateTo'] ?>"
                       class="form-control" required="required">


            </div>
            <button type='submit' name="showReport" value="1" class="btn btn-primary">показать отчет</button>

        </form>


        <?php

        if ($arResult["SHOW_REPORT"] && count($arResult["REPORT"])) {
            ?>

            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя сотрудника</th>
                    <th>email сотрудника</th>
                    <th>Отмечено времени, в часах</th>
                    <th>Отмечено времени( включая подчинненных), в часах</th>
                    <th>Дополнительно</th>
                </tr>
                </thead>
                <tbody>

                <?php $this->showRows($arResult["REPORT"], '') ?>

                </tbody>
            </table>

            <?php


        }

        $this->end();
    }

    function showRows($arRows, $startNumber)
    {
        $i = 0;
        foreach ($arRows as $arRow) {
            ?>
            <tr data-toggle="tooltip" title="<?php echo  htmlspecialchars($arRow[Base::USER_DESCRIPTION],ENT_QUOTES)?>">
                <th scope="row"><?= $startNumber . "" . (++$i) ?></th>
                <td><?php echo  $arRow[Base::USER_NAME] ?></td>
                <td><?php $email = $arRow[Base::USER_EMAIL];
                    if (!$arRow['isEmailValid']) {
                        ?><span class="text-danger"><?= $email ?> </span><?
                    }else{
                        ?><span ><?= $email ?></span><?
                    }
                    ?></td>
                <td><?php echo  number_format($arRow['time']/3600,2,","," " )?></td>
                <td><?php echo   number_format(($arRow['time']+$arRow['groupTime'])/3600 ,2,","," " )?></td>
                <td><?php if (isset($arRow['part-time-date']) && !empty($arRow['part-time-date'])) {
                        ?>
                        <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top"
                           title="<?php echo join($arRow['part-time-date'], ', ') ?>">недоработка</a>

                        <?php
                    } ?></td>
            </tr>
            <?php if (isset($arRow['children']) && !empty($arRow['children'])) {
                $this->showRows($arRow['children'], $startNumber . $i . ".");
            }
        }
    }

}