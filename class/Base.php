<?php

class Base
{

    /**
     * @var bool|PDO
     */
    protected $pdo = false;

    const USER_TABLE_NAME = 'users';
    const USER_ID = 'Id';
    const USER_NAME = 'Name';
    const USER_EMAIL = 'Email';
    const USER_EMPLOYER = 'Employer';
    const USER_DESCRIPTION = 'Info';

    const TIMESHEET_TABLE_NAME = 'Timesheet';
    const TIMESHEET_EMPLOYEE_ID = 'EmployeeId';
    const TIMESHEET_TIME = 'Time';
    const TIMESHEET_DATE = 'Date';

    /**
     * Base constructor.
     * @throws PDOException
     */
    function __construct()
    {
        $host = Params::DB_HOST;
        $dbName = Params::DB_NAME;
        $userName = Params::DB_USERNAME;
        $charset = Params::DB_CHARSET;
        $password = Params::DB_PASSWORD;
        $dsn = "mysql:host=$host;dbname=$dbName;charset=$charset";
        $this->pdo = new PDO($dsn, $userName, $password);

    }

    function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getAllTimeSheet(DateTime $from, DateTime $to)
    {

        $sql = ("SELECT * from  ".self::TIMESHEET_TABLE_NAME."  where :dateFrom <= " . self::TIMESHEET_DATE . " AND " . self::TIMESHEET_DATE . " <= :dateTo ");
        $sth = $this->getPdo()->prepare($sql);
        $sth->execute(
            [
                ":dateFrom"=> $from->format("Y-m-d H:i:s"),
                ":dateTo"=> $to->format("Y-m-d H:i:s")
            ]
        );

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     */
    public function getAllUsers()
    {
        $rsUsers = $this->getPdo()->query("SELECT * from  ".self::USER_TABLE_NAME);
        return $rsUsers->fetchAll(PDO::FETCH_ASSOC);
    }


}
