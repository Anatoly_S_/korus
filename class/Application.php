<?php

class Application
{
    protected $base = false;

    function __construct()
    {
        $this->base = new Base();

    }


    function getReport(DateTime $dateFrom, DateTime $dateTo)
    {
//users
        $arUsers = $this->base->getAllUsers();
        $arUsersById = array_column($arUsers, null, Base::USER_ID);
//init timelabel
        foreach ($arUsersById as $k => &$v) {
            $v['time'] = 0;
            $v['groupTime'] = 0;
            $email=$v[Base::USER_EMAIL];
            if (function_exists('idn_to_ascii') && strpos($email, '@') !== false) {
                $parts = explode('@', $email);
                $email = $parts[0].'@'.idn_to_ascii($parts[1]);
            }
            $v['isEmailValid']  = strlen(filter_var($email, FILTER_VALIDATE_EMAIL));

        }
        unset($v);

        //timeSheet
        $arTimeSheet = $this->base->getAllTimeSheet($dateFrom, $dateTo);

        foreach ($arTimeSheet as &$v) {
            if (isset($arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]])) {
                list($hours, $minutes, $seconds) = sscanf($v[Base::TIMESHEET_TIME], '%d:%d:%d');
                $time = $hours * 3600 + $minutes * 60 + $seconds;
                $arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]]["time"] += $time;
             //   $arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]]["groupTime"] += $time;
                if (!isset($arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]]["timeByDate"][$v[Base::TIMESHEET_DATE]])) {
                    $arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]]["timeByDate"][$v[Base::TIMESHEET_DATE]] = 0;
                }
                $arUsersById[$v[Base::TIMESHEET_EMPLOYEE_ID]]["timeByDate"][$v[Base::TIMESHEET_DATE]] += $time;

            }

        }
        //working hours read
        /**/
        $stepDate = $dateFrom;

        do {
            if (!in_array($stepDate->format("N"), [6, 7])) {//no saturday //no sunday
                $strDate = $stepDate->format("Y-m-d");
                foreach ($arUsersById as &$v) {
                    if(!isset($v['timeByDate'][$strDate])||$v['timeByDate'][$strDate]<28800){
                        $v['part-time-date'][]=$strDate;
                    }

                }
                unset($v);
            }

            $stepDate->modify('tomorrow');


        } while ($stepDate < $dateTo);
/**/
        //relations
        foreach ($arUsersById as $k => &$v) {
            if (isset($arUsersById[$v[Base::USER_EMPLOYER]])) {
                //install relations
                $arUsersById[$v[Base::USER_EMPLOYER]]['children'][$k] = &$v;
            }
            unset($v);
        }

        $arUsersById = array_filter($arUsersById, function ($array) {
            return $array[Base::USER_EMPLOYER] == 0;
        });

        //install time label
        self::getWorkForEmployee($arUsersById);


        return $arUsersById;

    }


    public static function getWorkForEmployee(&$arUsers)
    {
        $sum = 0;


        foreach ($arUsers as &$ar) {
            $sum += $ar['time'];
            if (isset($ar['children']) && is_array($ar['children'])) {
                $ar['groupTime'] = self::getWorkForEmployee($ar['children']);
                $sum += $ar['groupTime'];
            }

        }
        unset($ar);


        return $sum;
    }


    function resolve()
    {
        $arResult = [];
        $arResult["ERROR"] = [];

        $arResult["FIELDS"]['dateFrom'] = '';
        $arResult["FIELDS"]['dateTo'] = '';

        $showReport = false;
        if (isset($_REQUEST["showReport"], $_REQUEST["dateFrom"], $_REQUEST["dateTo"])) {
            $arResult["FIELDS"]['dateFrom'] = $_REQUEST["dateFrom"];
            $arResult["FIELDS"]['dateTo'] = $_REQUEST["dateTo"];


            $dateFrom = DateTime::createFromFormat("Y-m-d", $_REQUEST["dateFrom"]);
            $dateTo = DateTime::createFromFormat("Y-m-d", $_REQUEST["dateTo"]);
            if ($dateTo == false) {
                $arResult["ERROR"][] = "не верно указана дата 'до'";
            }

            if ($dateFrom == false) {
                $arResult["ERROR"][] = "не верно указана дата  в поле 'от'";
            }

            if ($dateFrom > $dateTo) {
                $arResult["ERROR"][] = "поле 'от' должно быть меньше поля 'до'";
            }
            if (empty($arResult["ERROR"])) {

                $dateFrom->modify('today');
                $arResult["REPORT"] = $this->getReport($dateFrom->modify('today'), $dateTo->modify('tomorrow'));
                $showReport = true;
            }

        }
        $arResult["SHOW_REPORT"] = $showReport;

        return $arResult;
    }

}