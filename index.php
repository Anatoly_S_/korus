<?php spl_autoload_register(function ($className) {
    include __DIR__ . "/class/" . $className . '.php';
});

try {
    $controller = new Application();

    $arResult = $controller->resolve();
    (new View())->resolve($arResult);
} catch (Exception $e) {
    ob_clean();
    (new View())->error($e);
}





